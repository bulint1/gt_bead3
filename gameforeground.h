#ifndef GAMEFOREGROUND_H
#define GAMEFOREGROUND_H

#include "columnwidget.h"
#include "gamebackground.h"
#include "gamepersistance.h"
#include "sizedialog.h"
#include <QMainWindow>

class gameForeground : public QWidget {
  Q_OBJECT
private:
  QVector<columnWidget *> column;
  gameBackground *Manager;
  gamepersistance *Pers;
  QHBoxLayout *gameTableLayout;
  QHBoxLayout *topLayout;
  QVBoxLayout *mainLayout;
  QPushButton *newGameButton;
  QPushButton *loadGameButton;
  QPushButton *saveGameButton;
  SizeDialog *dialog;
  void disableColumns();
  void generateDisplay(int width, int heigth);
  void updateColumn(int i);
private slots:
  void updateScreen();
  void colClicked();
  void gameState();
  void newGame();
  void gen();

public:
  gameForeground(QWidget *parent = nullptr);
  ~gameForeground();
};

#endif // GAMEFOREGROUND_H
