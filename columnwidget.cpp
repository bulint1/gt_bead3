#include "columnwidget.h"

int columnWidget::getInd() const { return ind; }

void columnWidget::setInd(int value) { ind = value; }

void columnWidget::setLabel(int i, QString text) { labels[i]->setText(text); }

void columnWidget::disable() { enabled = false; }

columnWidget::columnWidget(int size) {
  mainLayout = new QVBoxLayout;
  enabled = true;
  labels.resize(size);
  for (int i = 0; i < size; ++i) {
    labels[i] = new QLabel(" ");
    labels[i]->setStyleSheet("font:45pt;font-family:Times New Roman");
    labels[i]->setMinimumWidth(50);
    mainLayout->addWidget(labels[i]);
  }
  setLayout(mainLayout);
}

void columnWidget::mousePressEvent(QMouseEvent *ev) {
  if (enabled)
    emit mousePressed();
}

void columnWidget::reset() {
  foreach (QLabel *label, labels) { label->setText(" "); }
}
