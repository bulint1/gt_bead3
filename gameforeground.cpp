#include "gameforeground.h"
#include <QMessageBox>
#include <QShortcut>

void gameForeground::disableColumns() {
  foreach (columnWidget *col, column) { col->disable(); }
}

void gameForeground::generateDisplay(int width, int height) {

  setMinimumSize(Manager->getWidth() * 60, Manager->getHeigth() * 65);
  resize(0, 0);
  foreach (columnWidget *a, column) {
    gameTableLayout->removeWidget(a);
    delete a;
  }
  column.clear();
  // gameTableLayout = new QHBoxLayout;
  column.resize(width);
  for (int i = 0; i < width; ++i) {
    column[i] = new columnWidget(height);
    gameTableLayout->addWidget(column[i]);
    column[i]->setInd(i);
    connect(column[i], SIGNAL(mousePressed()), this, SLOT(colClicked()));
  }
}

void gameForeground::updateScreen() {
  for (int i = 0; i < column.size(); ++i)
    updateColumn(i);
}

void gameForeground::updateColumn(int i) {
  for (int j = 0; j < Manager->getHeigth(); ++j) {
    column[i]->setLabel(j, Manager->getCell(i, j));
  }
}

void gameForeground::colClicked() {
  int col = (dynamic_cast<columnWidget *>(QObject::sender()))->getInd();
  Manager->drop(col);
}

void gameForeground::gameState() {
  if (Manager->getWinner() == "X") {
    QMessageBox::information(this, "Game Over!", "X won!");
    disableColumns();
    newGame();
  } else if (Manager->getWinner() == "O") {
    QMessageBox::information(this, "Game Over!", "O won!");
    disableColumns();
    newGame();
  } else if (Manager->getWinner() == "Draw") {
    QMessageBox::information(this, "Game Over!", "Draw!");
    disableColumns();
    newGame();
  }
}

void gameForeground::newGame() {
  if (dialog->exec()) {
    Manager->newGame(dialog->getWidth(), dialog->getHeight());
    generateDisplay(dialog->getWidth(), dialog->getHeight());
  }
  // generateDisplay(Manager->getWidth(), Manager->getHeigth());

  updateScreen();
}

void gameForeground::gen() {
  generateDisplay(Manager->getWidth(), Manager->getHeigth());
}

gameForeground::gameForeground(QWidget *parent) : QWidget(parent) {
  setMinimumSize(400, 300);
  setWindowTitle("ConnectFour");
  gameTableLayout = new QHBoxLayout;
  topLayout = new QHBoxLayout;
  mainLayout = new QVBoxLayout;
  Manager = new gameBackground;
  // Pers = new gamepersistance(Manager);
  newGameButton = new QPushButton("New Game");
  saveGameButton = new QPushButton("Save Game");
  loadGameButton = new QPushButton("Load Game");
  dialog = new SizeDialog(this);
  Manager->newGame(8, 5);
  generateDisplay(8, 5);
  gameTableLayout->setSpacing(0);

  // Manager->newGame(8, 5);
  topLayout->addWidget(newGameButton);
  topLayout->addWidget(saveGameButton);
  topLayout->addWidget(loadGameButton);
  mainLayout->addLayout(topLayout);
  mainLayout->addLayout(gameTableLayout);
  connect(Manager, SIGNAL(updateScreen()), this, SLOT(updateScreen()));
  connect(Manager, SIGNAL(gameOver()), this, SLOT(gameState()));
  connect(newGameButton, SIGNAL(clicked()), this, SLOT(newGame()));
  connect(saveGameButton, SIGNAL(clicked()), Manager, SLOT(handle()));
  connect(loadGameButton, SIGNAL(clicked()), Manager, SLOT(handle()));
  connect(Manager, SIGNAL(regenerateTable()), this, SLOT(gen()));
  setLayout(mainLayout);
}

gameForeground::~gameForeground() {}
