#include "sizedialog.h"

SizeDialog::SizeDialog(QWidget *parent) : QDialog(parent) {
  setFixedSize(200, 100);
  setWindowTitle("Options");
  setModal(true);
  width = 8;
  height = 5;
  srand(time(nullptr));

  text = new QLabel("Select the size of the Game!");
  sizeS = new QPushButton("8x5");
  sizeM = new QPushButton("10x6");
  sizeL = new QPushButton("12x7");
  sizeRandom = new QPushButton("Random");
  cancel = new QPushButton("Cancel");
  connect(sizeS, &QPushButton::clicked, this, [this] { setter(8); });
  connect(sizeS, SIGNAL(clicked()), this, SLOT(accept()));
  connect(sizeM, &QPushButton::clicked, this, [this] { setter(10); });
  connect(sizeM, SIGNAL(clicked()), this, SLOT(accept()));
  connect(sizeL, &QPushButton::clicked, this, [this] { setter(12); });
  connect(sizeL, SIGNAL(clicked()), this, SLOT(accept()));
  connect(sizeRandom, &QPushButton::clicked, this, [this] { setter(42); });
  connect(sizeRandom, SIGNAL(clicked()), this, SLOT(accept()));

  gridLayout = new QGridLayout;
  mainLayout = new QVBoxLayout;
  gridLayout->addWidget(sizeS, 0, 0);
  gridLayout->addWidget(sizeM, 0, 1);
  gridLayout->addWidget(sizeL, 1, 0);
  gridLayout->addWidget(sizeRandom, 1, 1);
  mainLayout->addWidget(text);
  mainLayout->addLayout(gridLayout);

  setLayout(mainLayout);
}
void SizeDialog::setter(int n) {
  if (n == 42) {
    static const int options[] = {8, 10, 12}; //írd át hogíy jó legyen
    int random = rand() % 3;
    width = options[random];
  } else
    width = n;
  height = width / 2 + 1;
  this->close();
}

int SizeDialog::getHeight() const { return height; }

int SizeDialog::getWidth() const { return width; }
