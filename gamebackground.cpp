#include "gamebackground.h"
#include <QMessageBox>

gameBackground::gameBackground(QObject *parent) : QObject(parent) {
  playerTurn = "X";
  pers = new gamepersistance;
  // newGame(8, 5);
}

QString gameBackground::getCell(int x, int y) { return gameTable[x][y]; }

void gameBackground::setCell(int x, int y, QString val) {
  gameTable[x][y] = val;
}

int gameBackground::getWidth() const { return width; }

int gameBackground::getHeigth() const { return height; }

void gameBackground::changeTurn() {
  if (playerTurn == "X")
    playerTurn = "O";
  else
    playerTurn = "X";
}

QString gameBackground::checkDiagonalLeft() {
  for (int n = 0; n < 3; ++n) {
    for (int i = 0; i < width - 3; ++i) {
      QString last = "";
      int counter = 0;
      int j = 0;
      while (i + j < width && n + j < height) {
        QString element = gameTable[i + j][n + j];
        if (element == last) {
          counter++;
        } else {
          counter = 0;
          last = element;
        }
        if (counter == 3 && element != " ")
          return element;
        ++j;
      }
    }
  }
  return "";
}

QString gameBackground::checkVertical() {
  foreach (QVector<QString> a, gameTable) {
    QString last = "";
    int counter = 0;
    foreach (QString element, a) {
      if (element == last) {
        counter++;
      } else {
        counter = 0;
        last = element;
      }
      if (counter == 3 && element != " ")
        return element;
    }
  }
  return "";
}

QString gameBackground::checkHorizontal() {
  for (int i = 0; i < height; ++i) {
    QString last = "";
    int counter = 0;
    for (int j = 0; j < width; ++j) {
      QString element = gameTable[j][i];
      if (element == last) {
        counter++;
      } else {
        counter = 0;
        last = element;
      }
      if (counter == 3 && element != " ")
        return element;
    }
  }
  return "";
}

QString gameBackground::checkDiagonalRight() {
  for (int n = height - 1; n > 2; --n) {
    for (int i = 0; i < width - 3; ++i) {
      QString last = "";
      int counter = 0;
      int j = 0;
      while (i + j < width && n - j >= 0) {
        QString element = gameTable[i + j][n - j];
        if (element == last && element != " ") {
          counter++;
        } else {
          counter = 1;
          last = element;
        }
        if (counter == 4 && element != " ")
          return element;
        ++j;
      }
    }
  }
  return "";
}

void gameBackground::newGame(int w, int h) {
  playerTurn = "X";
  winner = "";
  gameTurn = 0;
  width = w;
  height = h;
  gameTable.clear();
  gameTable.resize(width);
  for (int i = 0; i < width; ++i) {
    gameTable[i].resize(height);
    for (int j = 0; j < height; ++j) {
      gameTable[i][j] = " ";
    }
  }
  // Pers->readData();
}

void gameBackground::checkGame() {
  if (checkHorizontal() == "X" || checkVertical() == "X" ||
      checkDiagonalRight() == "X" || checkDiagonalLeft() == "X") {
    winner = "X";
    emit gameOver();
  } else if (checkHorizontal() == "O" || checkVertical() == "O" ||
             checkDiagonalRight() == "O" || checkDiagonalLeft() == "O") {
    winner = "O";
    emit gameOver();
  } else if (gameTurn == width * height) {
    winner = "Draw";
    emit gameOver();
  }
  // emit updateScreen();
  // gameTurn = 0;
}

QString gameBackground::getWinner() const { return winner; }

void gameBackground::drop(int column) {

  if (gameTable[column][0] == " ") {
    int i = 0;
    while (i < height && gameTable[column][i] == " ")
      ++i;
    gameTable[column][i - 1] = playerTurn;
  }
  ++gameTurn;
  changeTurn();
  emit updateScreen();
  checkGame();
}

void gameBackground::handle() {

  if ((dynamic_cast<QPushButton *>(QObject::sender()))->text() ==
      "&Load Game") {
    pers->loadData(gameTable, height, width);
    emit regenerateTable();
    emit updateScreen();
  } else {
    pers->saveData(gameTable, height, width);
  }
}
