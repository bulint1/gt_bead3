QT += testlib
QT += widgets
QT += gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_testbackgroundandpersistance.cpp \
    gamebackground.cpp \
    gamepersistance.cpp

HEADERS += \
    gamepersistance.h \
    gamebackground.h
