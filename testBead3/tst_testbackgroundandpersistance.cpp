
#include <QtTest>

// add necessary includes here
#include "gamebackground.h"
#include "gamepersistance.h"
#include <QObject>
#include <QWidget>

class testBackgroundAndPersistance : public QObject {
  Q_OBJECT
  gameBackground *Manager;
  gamepersistance *pers;

public:
  testBackgroundAndPersistance();
  ~testBackgroundAndPersistance();

private slots:
  void initTestCase();
  void cleanupTestCase();
  void testStart();
  void testctor();
  void testnewGame();
  void testChangeTurn();
  void testDrop();
  void checkChecks();
  void testPers();
};

testBackgroundAndPersistance::testBackgroundAndPersistance() {}

testBackgroundAndPersistance::~testBackgroundAndPersistance() {}

void testBackgroundAndPersistance::initTestCase() {
  Manager = new gameBackground;
}

void testBackgroundAndPersistance::cleanupTestCase() { delete Manager; }

void testBackgroundAndPersistance::testStart() {}

void testBackgroundAndPersistance::testctor() {
  QVERIFY(Manager->playerTurn == "X");
}

void testBackgroundAndPersistance::testnewGame() {
  for (int i = 1; i < 10; ++i) {
    Manager->newGame(i, i * 2 + 1);
    QVERIFY(Manager->getHeigth() == i * 2 + 1);
    QVERIFY(Manager->getWidth() == i);
    foreach (QVector<QString> vect, Manager->gameTable)
      foreach (QString a, vect)
        QVERIFY(a == " ");
  }
}

void testBackgroundAndPersistance::testChangeTurn() {
  QVERIFY(Manager->playerTurn == "X");
  Manager->changeTurn();
  QVERIFY(Manager->playerTurn == "O");
}

void testBackgroundAndPersistance::testDrop() {
  for (int i = 0; i < Manager->getHeigth(); ++i) {
    QVERIFY(Manager->gameTable[0][Manager->getHeigth() - (i + 1)] == " ");
    Manager->drop(0);
    QVERIFY(Manager->gameTable[0][Manager->getHeigth() - (i + 1)] != " ");
  }
}

void testBackgroundAndPersistance::checkChecks() {
  Manager->newGame(8, 5);
  for (int i = 0; i < 4; ++i) {
    QVERIFY(Manager->checkVertical() == "");
    Manager->playerTurn = "X";
    Manager->drop(0);
  }
  QVERIFY(Manager->checkVertical() == "X");
  Manager->newGame(8, 5);
  for (int i = 0; i < 4; ++i) {
    QVERIFY(Manager->checkHorizontal() == "");
    Manager->playerTurn = "X";
    Manager->drop(i);
  }
  QVERIFY(Manager->checkHorizontal() == "X");
  Manager->newGame(8, 5);
  for (int i = 0; i < 4; ++i) {
    QVERIFY(Manager->checkDiagonalRight() == "");
    Manager->gameTable[i][Manager->getHeigth() - 1 - i] = "X";
  }
  QVERIFY(Manager->checkDiagonalRight() == "X");
  Manager->newGame(8, 5);
  for (int i = 0; i < 4; ++i) {
    QVERIFY(Manager->checkDiagonalLeft() == "");
    Manager->gameTable[i][i] = "X";
  }
  QVERIFY(Manager->checkDiagonalLeft() == "X");
}

void testBackgroundAndPersistance::testPers() {
  Manager->newGame(8, 5);
  for (int i = 0; i < Manager->getWidth(); ++i) {
    Manager->drop(i);
  }
  pers->saveData(Manager->gameTable, Manager->getHeigth(), Manager->getWidth());
  Manager->newGame(10, 25);
  pers->loadData(Manager->gameTable, Manager->height, Manager->width);
  QVERIFY(Manager->getHeigth() == 5);
  QVERIFY(Manager->getWidth() == 8);
  for (int i = 0; i < Manager->getHeigth(); ++i) {
    QVERIFY(Manager->gameTable[i][Manager->getHeigth() - 1] ==
            Manager->playerTurn);
    Manager->changeTurn();
  }
  Manager->newGame(5, 5);
  QProcess::execute("rm save.txt");
  pers->loadData(Manager->gameTable, Manager->height, Manager->width);
  QVERIFY(Manager->getHeigth() == 5);
  QVERIFY(Manager->getWidth() == 5);
  foreach (QVector<QString> vect, Manager->gameTable)
    foreach (QString cont, vect)
      QVERIFY(cont == " ");
}

QTEST_APPLESS_MAIN(testBackgroundAndPersistance)

#include "tst_testbackgroundandpersistance.moc"
