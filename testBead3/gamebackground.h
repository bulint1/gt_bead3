#ifndef GAMEBACKGROUND_H
#define GAMEBACKGROUND_H

//#include "columnwidget.h"
#include "gamepersistance.h"
#include <QObject>
#include <QString>
#include <QVector>

class gameBackground : public QObject {
  Q_OBJECT
public:
  explicit gameBackground(QObject *parent = nullptr);
  QString getCell(int x, int y);
  QString getWinner() const;
  void setCell(int x, int y, QString val);
  void newGame(int width, int height);
  void checkGame();
  int getWidth() const;
  int getHeigth() const;
  gamepersistance *pers;
  QVector<QVector<QString>> gameTable;
  QString playerTurn;
  QString winner;
  int width;
  int height;
  int gameTurn;
  void changeTurn();
  QString checkDiagonalRight();
  QString checkDiagonalLeft();
  QString checkHorizontal();
  QString checkVertical();
signals:
  void updateScreen();
  void gameOver();
  void regenerateTable();
public slots:
  void drop(int column);
  void handle();
};

#endif // GAMEBACKGROUND_H
