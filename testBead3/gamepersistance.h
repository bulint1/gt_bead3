#ifndef GAMEPERSISTANCE_H
#define GAMEPERSISTANCE_H
#include <QVector>
#include <QWidget>

class gamepersistance : public QWidget {
  Q_OBJECT
public:
  gamepersistance();
  void loadData(QVector<QVector<QString>> &gametable, int &h, int &w);
  void saveData(const QVector<QVector<QString>> &gametable, const int &h,
                const int &w);
};

#endif // GAMEPERSISTANCE_H
