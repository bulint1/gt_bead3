#ifndef COLUMNWIDGET_H
#define COLUMNWIDGET_H

#include <QEvent>
#include <QLabel>
#include <QLayout>
#include <QMouseEvent>
#include <QPushButton>
#include <QWidget>

class columnWidget : public QWidget {
  Q_OBJECT
private:
  QVector<QLabel *> labels;
  QVBoxLayout *mainLayout;
  int ind;
  bool enabled;

public:
  columnWidget(int size);
  void clicked();
  void mousePressEvent(QMouseEvent *ev);
  void reset();
  int getInd() const;
  void setInd(int value);
  void setLabel(int i, QString text);
  void disable();

signals:
  void mousePressed();
};

#endif // COLUMNWIDGET_H
