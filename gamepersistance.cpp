#include "gamepersistance.h"
#include <fstream>

gamepersistance::gamepersistance() {}

void gamepersistance::loadData(QVector<QVector<QString>> &gametable, int &h,
                               int &w) {
  std::string file = "save.txt";
  std::ifstream f(file.c_str());
  if (f.fail()) {
    // QMessageBox::information(this, "Error", "Map not found!");
    return;
  }
  f >> w;
  f >> h;
  gametable.clear();
  gametable.resize(w);

  for (int i = 0; i < w; ++i) {
    gametable[i].resize(h);
    for (int j = 0; j < h; ++j) {
      char in;
      f >> in;
      if (in == 'S')
        gametable[i][j] = QString(" ");
      else
        gametable[i][j] = QString(in);
    }
  }
}

void gamepersistance::saveData(const QVector<QVector<QString>> &gametable,
                               const int &h, const int &w) {
  std::string file = "save.txt";
  std::ofstream f(file.c_str());
  f << w;
  f << " ";
  f << h;
  f << '\n';
  for (int i = 0; i < w; ++i) {
    for (int j = 0; j < h; ++j) {
      if (gametable[i][j] == " ")
        f << "S";
      else
        f << gametable[i][j].toStdString();
    }
  }
}
