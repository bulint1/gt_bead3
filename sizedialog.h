#ifndef GRIDSIZEDIALOG_H
#define GRIDSIZEDIALOG_H

#include <QDialog>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>

class SizeDialog : public QDialog {
  Q_OBJECT
public:
  explicit SizeDialog(QWidget *parent = nullptr);
  int getWidth() const;
  int getHeight() const;

private slots:
  void setter(int n);

private:
  int width;
  int height;
  QLabel *text;
  QVBoxLayout *mainLayout;
  QPushButton *sizeS;
  QPushButton *sizeM;
  QPushButton *sizeL;
  QPushButton *sizeRandom;
  QPushButton *cancel;
  QGridLayout *gridLayout;
};

#endif // GRIDSIZEDIALOG_H
